#TODO: test for a couple known vectors

r = 1
angles = [pi]

@test (HoughTransform.hyperspherical2cartesian(r, angles); true)
cartvec = HoughTransform.hyperspherical2cartesian(r, angles)
@test (HoughTransform.cartesian2hyperspherical(cartvec); true)
r2, angles2 = HoughTransform.cartesian2hyperspherical(cartvec)
@test r == r2
@test isapprox(angles, angles2)

r = 2
angles = [pi / 3]

cartvec = HoughTransform.hyperspherical2cartesian(r, angles)
r2, angles2 = HoughTransform.cartesian2hyperspherical(cartvec)
@test r == r2
@test isapprox(angles, angles2)

cartvec = ones(3)
r2, angles2 = HoughTransform.cartesian2hyperspherical(cartvec)
cartvec2 = HoughTransform.hyperspherical2cartesian(r2, angles2)
@test isapprox(cartvec, cartvec2)

cartvec = [1, 2, -3, 0]
r2, angles2 = HoughTransform.cartesian2hyperspherical(cartvec)
cartvec2 = HoughTransform.hyperspherical2cartesian(r2, angles2)
@test isapprox(cartvec, cartvec2, atol = 1e-9)

cartvec = [-1, 2, -3, 0]
r2, angles2 = HoughTransform.cartesian2hyperspherical(cartvec)
cartvec2 = HoughTransform.hyperspherical2cartesian(r2, angles2)
@test isapprox(cartvec, cartvec2, atol = 1e-9)
