#TODO: test with known plane sets of points and test the outputs

points = rand(4, 20)

@test (
    HoughTransform.linearhoughtransform(points, angle_step = pi / 20, distance_step = 0.05); true
)
bins, rs, angles =
    HoughTransform.linearhoughtransform(points, angle_step = pi / 20, distance_step = 0.05)

image = rand(10, 20, 30)

@test (HoughTransform.linearhoughtransformimage(image); true)
bins, rs, angles = HoughTransform.linearhoughtransformimage(image)
