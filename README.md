# HoughTransform

## About

`HoughTransform` is a package that implements the linear Hough transform,
useful in identifying linear features in a set
of points or arrays of values in arbitrary dimensional spaces. If you are looking
for feature detection in images then you may want to use
[`ImageFeatures`](https://github.com/JuliaImages/ImageFeatures.jl)
instead.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/HoughTransform.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for HoughTransform.jl](https://pawelstrzebonski.gitlab.io/HoughTransform.jl/).
