import Combinatorics
import LinearAlgebra

# https://en.wikipedia.org/wiki/Hough_transform
#TODO: shift/normalize points before analyzing?
#TODO: Integrate all of these functions together?
#TODO: Allow for use of sparse arrays?
#TODO: Add functions for identifying the planes in the bins and converting back to normal-distance form

"""
    linearhoughtransform(
		points::AbstractMatrix;
		distance_step::Number = 1,
		angle_step::Number = pi/100,
		maxsize::Number = 1e8,
	)->(bins, rs, angles)

Given an array of points, `points[:, i]'`, calculate the Hough
transform `bins` for the distances from origin `r` and hyperspherical
angles `angles`. Returns a matrix of plane tallies
`bin[r_index, angle_list_index]` and the corresponding distance list `rs`
and list of hyperspherical angle vectors `angles`.

The sample spacing for the plane distance and angles are defined by
`distance_step` and `angle_step` respectively.

If the `bins` array will be larger than `maxsize` bytes, then an
error will thrown.
"""
function linearhoughtransform(
    points::AbstractMatrix;
    Nsamples::Integer = 32,
    distance_step::Number = 1,
    angle_step::Number = pi / 100,
    maxsize::Number = 1e8,
)
    Ndim, Npoints = size(points)
    rmax = maximum([LinearAlgebra.norm(points[:, i]) for i = 1:size(points, 2)]) + 1

    # Calculate the ranges for the distance and angles
    rrng = -rmax:distance_step:rmax
    dr = rrng[2] - rrng[1]
    anglerng = 0:angle_step:pi

    # Create a list of all possible hyperspherical angles
    anglelist = Combinatorics.combinations(anglerng, Ndim - 1)

    # Instantiate the line bin array
    binsize = 8 * length(rrng) * length(anglelist)
    binsize > maxsize ? error("Size of `bins` would be ", binsize) :
    @info string("Size of `bins` is ", binsize)
    bins = zeros(length(rrng), length(anglelist))

    # Pre-calculate all of the possible normal vectors and convert to a
    # matrix that can be multiplied by a point to calculate the distance
    normals = reduce(hcat, hyperspherical2cartesian.(1, anglelist))'

    # Pre-allocate distance arrays
    distances = zeros(length(anglelist))
    distanceidxs = zeros(Int, length(anglelist))

    for i = 1:Npoints
        # Calculate the distance of the various planes
        distances .= normals * points[:, i]
        # Convert the real distance to an index in the bins array
        distanceidxs .= round.(Int, (distances .+ rmax) ./ dr) .+ 1
        # Increment the bin count for the appropriate distances
        for (j, d) in enumerate(distanceidxs)
            bins[d, j] += 1
        end
    end

    bins, rrng, anglelist
end

"""
    cart2vec(x::CartesianIndex)

Convert a CartesianIndex to a vector.
"""
cart2vec(x::CartesianIndex) = collect(Tuple(x))

"""
    linearhoughtransformimage(image::AbstractArray; distance_step::Number=1, angle_step=nothing, maxsize::Number=1e8)->(bins, rs, angles)

Given an arbitrarily dimensional `image`, calculate the Hough
transform `bins` for the distances from origin `rs` and hyperspherical
angles `angles`. Returns a matrix of plane "tallies" (the value
of each point in `image` determines the weight of each point or
plane through that point)
`bin[r_index, angle_list_index]` and the corresponding distance list `rs`
and list of hyperspherical angle vectors `angles`. 

The sample spacing for the plane distance and angles are defined by
`distance_step` and `angle_step` respectively.

If the `bins` array will be larger than `maxsize` bytes, then an
error will thrown.
"""
function linearhoughtransformimage(
    image::AbstractArray;
    distance_step::Number = 1,
    angle_step = nothing,
    maxsize::Number = 1e8,
)
    Ndim, Npoints = ndims(image), length(image)
    rmax = LinearAlgebra.norm(size(image)) + 1

    # Calculate the ranges for the distance and angles
    rrng = -rmax:distance_step:rmax
    anglerng =
        isnothing(angle_step) ? LinRange(0, pi, minimum(size(image))) : 0:angle_step:pi

    # Create a list of all possible hyperspherical angles
    anglelist = Combinatorics.combinations(anglerng, Ndim - 1)

    # Instantiate the line bin array
    binsize = 8 * length(rrng) * length(anglelist)
    binsize > maxsize ? error("Size of `bins` would be ", binsize) :
    @info string("Size of `bins` is ", binsize)
    bins = zeros(length(rrng), length(anglelist))

    # Pre-calculate all of the possible normal vectors and convert to a
    # matrix that can be multiplied by a point to calculate the distance
    normals = reduce(hcat, hyperspherical2cartesian.(1, anglelist))'

    # Pre-allocate distance arrays
    distances = zeros(length(anglelist))
    distanceidxs = zeros(Int, length(anglelist))

    for i in CartesianIndices(size(image))
        # Calculate the distance of the various planes
        distances .= normals * cart2vec(i)
        # Convert the real distance to an index in the bins array
        distanceidxs .= round.(Int, (distances .+ rmax) ./ distance_step) .+ 1
        # Increment the bin count for the appropriate distances
        for (j, d) in enumerate(distanceidxs)
            bins[d, j] += image[i]
        end
    end

    bins, rrng, anglelist
end
