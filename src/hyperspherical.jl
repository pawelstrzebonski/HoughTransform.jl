import LinearAlgebra

# https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
#TODO: Refactor based on Wiki's equations?

"""
    hyperspherical2cartesian(r, angles)

Calculate the Cartesian vector defined by the hyperspherical coordinate
radius `r` and angles `angles`.
"""
function hyperspherical2cartesian(r, angles)
    Ndims = length(angles) + 1
    cartvec = r .* ones(Ndims)
    for i = 1:(Ndims-1)
        cartvec[i] *= cos(angles[i])
        cartvec[(i+1):end] .*= sin(angles[i])
    end
    reverse(cartvec)
end

"""
    cartesian2hyperspherical(cartvec)->(r, angles)

Calculate the hyperspherical coordinate radius `r` and angles `angles`
for a given Cartesian vector.
"""
function cartesian2hyperspherical(cartvec)
    Ndims = length(cartvec)
    angles = ones(Ndims - 1)
    r = LinearAlgebra.norm(cartvec)
    unitvec = reverse(cartvec ./ r)
    for i = 1:(Ndims-2)
        angles[i] = acos(unitvec[i])
        unitvec[(i+1):end] ./= sin(angles[i])
    end
    angles[end] = atan(unitvec[end], unitvec[end-1])
    r, angles
end
