# HoughTransform.jl Documentation

`HoughTransform` is a package that implements the linear Hough transform,
useful in identifying linear features in a set
of points or arrays of values in arbitrary dimensional spaces. If you are looking
for feature detection in images then you may want to use
[`ImageFeatures`](https://github.com/JuliaImages/ImageFeatures.jl)
instead.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/HoughTransform.jl
```

## Features

* Arbitrary dimension linear Hough transform on a list of points
* Arbitrary dimension linear Hough transform on an arbitrary dimensional image/array
