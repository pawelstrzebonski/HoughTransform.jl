# Hyperspherical Coordinate Systems

Implement conversing arbitrary dimensional vectors between Cartesian
coordinate systems and hyperspherical coordinate systems.

```@autodocs
Modules = [HoughTransform]
Pages   = ["hyperspherical.jl"]
```
