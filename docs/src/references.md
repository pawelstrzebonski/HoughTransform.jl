# References

A description of the Hough transform can be found on the
[Wikipedia article](https://en.wikipedia.org/wiki/Hough_transform).
There are also many other informative descriptions of the basic method
and implementation, such as
[https://alyssaq.github.io/2014/understanding-hough-transform/](https://alyssaq.github.io/2014/understanding-hough-transform/)
and
[https://moonbooks.org/Articles/Implementing-a-simple-python-code-to-detect-straight-lines-using-Hough-transform/](https://moonbooks.org/Articles/Implementing-a-simple-python-code-to-detect-straight-lines-using-Hough-transform/).

Hough transforms involve formulating hyperplanes (usually lines in 2D and
planes in 3D) in the
[Hesse normal form](https://en.wikipedia.org/wiki/Hesse_normal_form) where
a plane is defined by the surface unit normal vector and the minimal distance
to the origin. This form turns out to be rather convenient when generalizing
to higher dimensions. Furthermore, the unit normal vector is defined
in terms of angles (as this convention is most convenient for the Hough
transform). This means using the common
[polar](https://en.wikipedia.org/wiki/Polar_coordinate_system)
or
[spherical coordinate system](https://en.wikipedia.org/wiki/Spherical_coordinate_system)
conventions for 2D and 3D problems, but for higher
dimensional problems we must use some form of a
["hyperspherical" coordinate system](https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates).
