# Example Usage

## List of Points

Given an array of points we can calculate the linear Hough transform
using the `linearhoughtransform` function:

```@example ex1
import HoughTransform

# Create an array for the points on a line
x=100*ones(100)
y=100*rand(100)
z=100*rand(100)
points=[x y z]'

# Obtain the Hough transform
bins, rs, angles=HoughTransform.linearhoughtransform(points)

nothing # hide
```

The array `bins` contains the Hough transform tally, `rs` the distances
for the Hough transform, and `angles` is a combination generator
for the hyperspherical angles for the transform. The elements of `bins`
with the greatest values should correspond to the planes in the provided
points:

```@example ex1
# Find the index of the maximal value
planeidx=findmax(bins)[2]

# Find the indices of the distance and angle arrays
ridx, angleidx=Tuple(planeidx)

# Find the distance and angle for the found plane
r, a=rs[ridx], collect(angles)[angleidx]
```

The plane is described in Hesse normal form, with the normal vector being
described by the (hyper-)spherical angles. We can obtain the Cartesian
normal vector using the `hyperspherical2cartesian` function:

```@example ex1
# Obtain the unit normal vector from the obtained list of angles
normalvec=HoughTransform.hyperspherical2cartesian(1, a)
```

The resulting planes are somewhat approximate, and the precision in the
plane distance and angles can be modified by setting the `distance_step`
and `angle_step` arguments to `linearhoughtransform`.

## Images/Arrays

Given an array of point values we can calculate the linear Hough transform
using the `linearhoughtransformimage` function. It's usage is largely
similar to the `linearhoughtransform` function described above:

```@example ex1
import HoughTransform

# Create an array for the points on a line
image=zeros(100, 100)
[image[101-i, i]=1 for i=1:100]

# Obtain the Hough transform
bins, rs, angles=HoughTransform.linearhoughtransformimage(image)

nothing # hide
```

```@example ex1
# Find the index of the maximal value
planeidx=findmax(bins)[2]

# Find the indices of the distance and angle arrays
ridx, angleidx=Tuple(planeidx)

# Find the distance and angle for the found plane
r, a=rs[ridx], collect(angles)[angleidx]
```

```@example ex1
# Obtain the unit normal vector from the obtained list of angles
normalvec=HoughTransform.hyperspherical2cartesian(1, a)
```
