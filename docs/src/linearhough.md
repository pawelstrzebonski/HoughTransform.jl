# Linear Hough Transform

Implement arbitrary dimensional linear Hough transforms for a set of
points or an array "image".

```@autodocs
Modules = [HoughTransform]
Pages   = ["linearhough.jl"]
```
