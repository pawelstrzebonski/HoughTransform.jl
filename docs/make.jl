using Documenter
import HoughTransform

makedocs(
    sitename = "HoughTransform.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "HoughTransform.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "hyperspherical.jl" => "hyperspherical.md",
            "linearhough.jl" => "linearhough.md",
        ],
        "test/" => [
            "hyperspherical.jl" => "hyperspherical_test.md",
            "linearhough.jl" => "linearhough_test.md",
        ],
    ],
)
